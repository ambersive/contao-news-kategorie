<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package News
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Contao;

class NewsCategories extends \Frontend
{
    public function parseArticles($objTemplate){

        if($objTemplate->pid !== null){
            $archive = \NewsArchiveModel::findBy('id',$objTemplate->pid);
            if($archive!=null){
                if($archive->image !== null){
                    if ($archive->image != '')
                    {
                        $objModel = \FilesModel::findByUuid($archive->image);
                        if (is_file(TL_ROOT . '/' . $objModel->path))
                        {
                            // Do not override the field now that we have a model registry (see #6303)
                            $arrArticle = $archive->row();

                            // Override the default image size
                            if ($archive->imageSize != '')
                            {
                                $size = deserialize($archive->imageSize);
                                $objTemplate->categoryImageThumb = Image::get($objModel->path, $size[0], $size[1], $size[2]);
                            } else {
                                $objTemplate->categoryImageThumb = $objModel->path;
                            }

                            $objTemplate->categoryName  = $archive->title;
                            $objTemplate->categoryImage = $objModel->path;
                        }
                    }
                }
            }
        }

        return $objTemplate;
    }
}