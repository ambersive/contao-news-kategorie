<?php

$GLOBALS['TL_DCA']['tl_module']['palettes']['newsmenu_categories']    = '{title_legend},name,headline,type;{config_legend},news_archives,news_showQuantity;{redirect_legend},jumpTo;{template_legend:hide},customTpl;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space';


// Anpassung der Palette
$GLOBALS['TL_DCA']['tl_module']['palettes']['newslist'] = str_replace
(
    'customTpl',
    'customTpl,news_category_useage',
    $GLOBALS['TL_DCA']['tl_module']['palettes']['newslist']
);

$GLOBALS['TL_DCA']['tl_module']['fields']['news_category_useage'] = array
(
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['news_category_useage'],
    'exclude'                 => true,
    'filter'                  => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "char(1) NOT NULL default ''"
);