<?php

/**
 * Load tl_content language file
 */
System::loadLanguageFile('tl_content');

// Anpassung der Palette
$GLOBALS['TL_DCA']['tl_news_archive']['palettes']['default'] = str_replace
(
    'jumpTo',
    'jumpTo,image,imageSize',
    $GLOBALS['TL_DCA']['tl_news_archive']['palettes']['default']
);

/**
 * Fields
 */

$GLOBALS['TL_DCA']['tl_news_archive']['fields']['image'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_news_archive']['image'],
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('filesOnly'=>true, 'extensions'=>Config::get('validImageTypes'), 'fieldType'=>'radio', 'mandatory'=>true),
    'sql'                     => "binary(16) NULL"
);

$GLOBALS['TL_DCA']['tl_news_archive']['fields']['imageSize'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'exclude'                 => true,
    'inputType'               => 'imageSize',
    'options'                 => $GLOBALS['TL_CROP'],
    'reference'               => &$GLOBALS['TL_LANG']['MSC'],
    'eval'                    => array('rgxp'=>'digit', 'nospace'=>true, 'helpwizard'=>true, 'tl_class'=>'long'),
    'sql'                     => "varchar(64) NOT NULL default ''"
);

class tl_news_archive_category extends Backend
{

    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
    }
}