<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package News
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'Contao\NewsCategories'              => 'system/modules/news_category/classes/NewsCategories.php',
    // Models
    'Contao\NewsModel'                   => 'system/modules/news_category/models/NewsModel.php',
    // Modules
    'Contao\ModuleNewsMenuCategories'    => 'system/modules/news_category/modules/ModuleNewsMenuCategories.php',
    'Contao\ModuleNewsList'              => 'system/modules/news_category/modules/ModuleNewsListCategories.php'

));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
    'mod_newsmenu_categories'      => 'system/modules/news_category/templates/modules',
    'news_latest_category'         => 'system/modules/news_category/templates/views',
));
