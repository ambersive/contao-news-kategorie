<?php

/** MODULES **/

$GLOBALS['FE_MOD']['news']['newsmenu_categories'] = "ModuleNewsMenuCategories";

/**
 * Register hook to add news items to the indexer
 */
$GLOBALS['TL_HOOKS']['parseArticles'][] = array('NewsCategories', 'parseArticles');
