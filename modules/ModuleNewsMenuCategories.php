<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package News
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Contao;


/**
 * Class ModuleNewsMenu
 *
 * Front end module "news archive".
 * @copyright  Leo Feyer 2005-2014
 * @author     Leo Feyer <https://contao.org>
 * @package    News
 */
class ModuleNewsMenuCategories extends \ModuleNews
{

	/**
	 * Current date object
	 * @var integer
	 */
	protected $Date;

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_newsmenu_categories';


	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new \BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### ' . utf8_strtoupper($GLOBALS['TL_LANG']['FMD']['newsmenu_categories'][0]) . ' ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		$this->news_archives = $this->sortOutProtected(deserialize($this->news_archives));

		if (!is_array($this->news_archives) || empty($this->news_archives))
		{
			return '';
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{
        $this->compileMenu();

		$this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyList'];
	}

	/**
	 * Generate the monthly menu
	 */
	protected function compileMenu()
	{

        global $objPage;
        $currentSite = $objPage->row();

        $time = time();
		$arrData = array();

		// Get the dates
        $objects = $this->Database->query("SELECT t1.id,t1.title,t1.jumpTo FROM tl_news_archive t1  WHERE t1.id IN(" . implode(',', array_map('intval', $this->news_archives)) . ")");
		while ($objects->next())
		{
			$arrData[$objects->id] = array('title'=>$objects->title,'count'=>$objects->total);
		}

		$strUrl = '';
		$arrItems = array();

        if($this->jumpTo){
            $objTarget = \PageModel::findBy('id',$this->jumpTo);
        } else {
            $objTarget = \PageModel::findBy('id',$currentSite['id']);

        }

        if($objTarget !== null){
            $strUrl = $this->generateFrontendUrl($objTarget->row());
        }

		// Prepare the navigation

        foreach ($arrData as $key=>$value)
        {
            $searchArr = array('published'=>1,'pid'=>$key);

            $arrItems[$key]['title'] = $value['title'];
            $arrItems[$key]['count'] = \NewsModel::countEntries($searchArr);
            $arrItems[$key]['link']  = $key;

        }

		$this->Template->items = $arrItems;
		$this->Template->showQuantity = ($this->news_showQuantity != '') ? true : false;
		$this->Template->url = $strUrl . (\Config::get('disableAlias') ? '&amp;' : '?');
		$this->Template->activeCategory = \Input::get('category');
	}
}
