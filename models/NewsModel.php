<?php
/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Contao;

class NewsModel extends \Model
{

    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_news';

    public static function countEntries(array $arrParams=array(),array $arrOptions=array())
    {
        if (!is_array($arrParams))
        {
            return 0;
        } else {
            $t = static::$strTable;
            $arrColumns = array();
            if(empty($arrParams)){
                $arrColumns[] = "$t.id!=0";
            } else {
                foreach($arrParams as $key => $value){
                    switch($key){
                        case 'IN':
                            $implode = implode(',', array_map('intval', $value['IN'][1]));
                            $arrColumns[] = $t.'.'.$value["IN"][0].' IN('.$implode.')';
                            break;
                        case 'LIKE':
                            $arrColumns[] = "$t.$value[0] $key '%$value[1]%'";
                            break;
                        case 'NOT':
                            $arrColumns[] = "$t.$value[0]!=$value[1]";
                            break;
                        case 'FROM_UNIXTIME':
                            $arrColumns[] = "FROM_UNIXTIME($t.$value[0],$value[1])=$value[2]";
                            break;
                        case 'BETWEEN':
                            $arrColumns[] = "$t.$value[0] BETWEEN $value[1] AND $value[2]";
                            break;
                        default:
                            $arrColumns[] = "$t.$key='$value'";
                            break;
                    }
                }
            }
            return static::countBy($arrColumns, null,$arrOptions);
        }
    }

    public static function sumEntries(array $arrParams=array(),array $arrOptions=array())
    {
        if (!is_array($arrParams))
        {
            return 0;
        } else {
            $t = static::$strTable;
            $arrColumns = array();
            if(empty($arrParams)){
                $arrColumns[] = "$t.id!=0";
            } else {
                foreach($arrParams['where'] as $key => $value){
                    switch($key){
                        case 'IN':
                            $implode = implode(',', array_map('intval', $value['IN'][1]));
                            $arrColumns[] = $t.'.'.$value["IN"][0].' IN('.$implode.')';
                            break;
                        case 'LIKE':
                            $arrColumns[] = "$t.$value[0] $key '%$value[1]%'";
                            break;
                        case 'NOT':
                            $arrColumns[] = "$t.$value[0]!=$value[1]";
                            break;
                        case 'FROM_UNIXTIME':
                            $arrColumns[] = "FROM_UNIXTIME($t.$value[0],$value[1])=$value[2]";
                            break;
                        case 'BETWEEN':
                            $arrColumns[] = "$t.$value[0] BETWEEN $value[1] AND $value[2]";
                            break;
                        default:
                            $arrColumns[] = "$t.$key='$value'";
                            break;
                    }
                }
            }
            return static::sumBy($arrColumns, $arrOptions,$arrParams['fields']);
        }
    }

    public static function sumBy($arrColumns,$arrOptions,$strFields=null){
        $t = static::$strTable;
        if($strFields==null){
            $strFields= "id";
        }
        $strQuery = "SELECT SUM(".$strFields.") AS sum FROM " . $t;
        if(!empty($arrColumns)){
            $strQuery .= ' WHERE ';
            $strQuery .= implode(' AND ',$arrColumns);
        }
        $data = \Database::getInstance()->prepare($strQuery)->execute();
        if($data->sum == null){$data->sum = 0;}
        return $data->sum;
    }

    /**
     * Find published news items by their parent ID and ID or alias
     *
     * @param mixed $varId      The numeric ID or alias name
     * @param array $arrPids    An array of parent IDs
     * @param array $arrOptions An optional options array
     *
     * @return \Model|null The NewsModel or null if there are no news
     */
    public static function findPublishedByParentAndIdOrAlias($varId, $arrPids, array $arrOptions=array())
    {
        if (!is_array($arrPids) || empty($arrPids))
        {
            return null;
        }

        $t = static::$strTable;
        $arrColumns = array("($t.id=? OR $t.alias=?) AND $t.pid IN(" . implode(',', array_map('intval', $arrPids)) . ")");

        if (!BE_USER_LOGGED_IN)
        {
            $time = time();
            $arrColumns[] = "($t.start='' OR $t.start<$time) AND ($t.stop='' OR $t.stop>$time) AND $t.published=1";
        }

        return static::findBy($arrColumns, array((is_numeric($varId) ? $varId : 0), $varId), $arrOptions);
    }

    public static function findEntries($arrParams, $intLimit=0, $intOffset=0, array $arrOptions=array())
    {

        if (!is_array($arrParams))
        {
            return null;
        }

        $t = static::$strTable;
        $arrColumns = array();
        if(empty($arrParams)){
            $arrColumns[] = "$t.id!=0";
        } else {
            foreach($arrParams as $key => $value){
                switch($key){
                    case 'IN':
                        $implode = implode(',', array_map('intval', $value['IN'][1]));
                        print_r($implode);
                        $arrColumns[] = $t.'.'.$value["IN"][0].' IN('.$implode.')';
                        print_r($arrColumns);
                        break;
                    case 'LIKE':
                        $arrColumns[] = "$t.$value[0] $key '%$value[1]%'";
                        break;
                    case 'NOT':
                        $arrColumns[] = "$t.$value[0]!=$value[1]";
                        break;
                    case 'FROM_UNIXTIME':
                        $arrColumns[] = "FROM_UNIXTIME($t.$value[0],$value[1])=$value[2]";
                        break;
                    case 'BETWEEN':
                        $arrColumns[] = "$t.$value[0] BETWEEN $value[1] AND $value[2]";
                        break;
                    default:
                        $arrColumns[] = "$t.$key='$value'";
                        break;
                }
            }
        }
        $options = array();
        if (!isset($arrOptions['order'])){
            $options['order']  = "$t.id ASC";
        } else {
            $options['order'] = "$t.".$arrOptions['order'][0]." ".$arrOptions['order'][1]."";
        }

        $options['limit']  = $intLimit;
        $options['offset'] = $intOffset;

        return static::findBy($arrColumns, null, $options);

    }

    public static function findEntryByIdOrAlias($varId=null, array $arrOptions=array(),array $searchArr=array())
    {
        if ($varId==null)
        {
            return null;
        }

        $t = static::$strTable;
        $arrColumns = array("($t.id=? OR $t.alias=?)");
        if(!empty($searchArr)){
            foreach($searchArr as $key => $value){
                switch($key){
                    case 'IN':
                        $implode = implode(',', array_map('intval', $value['IN'][1]));
                        $arrColumns[] = $t.'.'.$value["IN"][0].' IN('.$implode.')';
                        break;
                    case 'LIKE':
                        $arrColumns[] = "$t.$value[0] $key '%$value[1]%'";
                        break;
                    case 'NOT':
                        $arrColumns[] = "$t.$value[0]!=$value[1]";
                        break;
                    case 'FROM_UNIXTIME':
                        $arrColumns[] = "FROM_UNIXTIME($t.$value[0],$value[1])=$value[2]";
                        break;
                    case 'BETWEEN':
                        $arrColumns[] = "$t.$value[0] BETWEEN $value[1] AND $value[2]";
                        break;
                    default:
                        $arrColumns[] = "$t.$key='$value'";
                        break;
                }
            }
        }
        return static::findBy($arrColumns, array((is_numeric($varId) ? $varId : 0), $varId), $arrOptions);
    }

}