# Newscategory for Contao 3.x+ #

by Ambersive.com

Bietet die Möglichkeit an ein Kategoriebild zu einem Newsarchiv hinzuzufügen.
Zusätzlich dazu besteht die Möglichkeit in der Nachrichtenliste die entsprechende Kategorie zu filtern.


### Versions ###

2014-11-26: Initial Commit